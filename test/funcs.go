package test

// ByPointer 使用指针交换两个变量的值
func ByPointer(a, b *int) {

	// 取a的值赋值给临时变量temp
	temp := *a

	// 取b指针的值赋给a指针
	*a = *b

	// 取temp的值赋值给b指针
	*b = temp

}

// CopySlice 复制切片元素
func CopySlice(srcData []byte) []byte {

	elementCount := len(srcData)
	// 预分配足够多的元素切片
	copyData := make([]byte, elementCount)

	// 调用内置的copy函数将srcData切片中的元素复制到copyData
	copy(copyData, srcData)

	return copyData
}
