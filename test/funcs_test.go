package test

import (
	"math/rand"
	"testing"
)

func TestByPointer(t *testing.T) {
	rand.Seed(86)

	x := rand.Intn(100)
	tempX := x

	y := rand.Intn(100)
	tempY := y

	ByPointer(&x, &y)

	if tempX != y {
		t.Errorf("tempX = %d; want %d", tempX, y)
	} else if tempY != x {
		t.Errorf("tempY = %d; want %d", tempY, x)
	}

}

func TestCopySlice(t *testing.T) {
	// 设置切片元素数量
	const elementCount = 30

	srcData := make([]byte, elementCount)
	// 生成随机数给切片赋值
	rand.Read(srcData)

	res := CopySlice(srcData)

	if (srcData == nil) != (res == nil) {
		t.Error()
	}

	if len(res) != len(srcData) {
		t.Errorf("len(res) = %d, len(srcData) = %d", len(res), len(srcData))
	}

	for i, v := range srcData {
		if v != res[i] {
			t.Errorf("res = %v,want = %v", res, srcData)
		}
	}
}
